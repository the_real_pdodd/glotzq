"""
Helper functions to fetch the data from the scheduler
"""
import time
import numpy as np
import pandas as pd
import logging
logger = logging.getLogger("glotzq.{}".format(__name__));

def _check_mode(mode):
    if mode and mode not in ['r', 'i', 'b', 'c']:
        raise RuntimeError("mode {!r} is not known".format(mode));

def _query(mode, account, cache):
    import subprocess
    _check_mode(mode);

    if mode is None:
        mode = ""
    else:
        mode = "-{}".format(mode);

    cmd = ["showq", "{mode}".format(mode=mode), "-g", "-w", "acct={account}".format(account=account), "--xml"];
    if cache:
        cmd.append("--noblock");
    else:
        cmd.append("--blocking");
    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE);
    return proc.stdout.read();

def _get_val(element, keys, dtype=str, default=None, strict=True, warn=True):
    for key in keys:
        if key in element.attributes.keys():
            return dtype(element.attributes[key].value);
    if strict:
        raise AttributeError("{} are not in the xml attributes".format(keys));
    if warn:
        print("Warning {} are not found in the xml".format(keys));
    return default;

def _parse_jobs(xmldata):
    jobs = xmldata.getElementsByTagName('job');
    data = dict(
                State=[],
                JobName=[],
                JobId=[],
                WalltimeRequested=[],
                WalltimeDuration=[],
                WalltimeRemaining=[],
                StartTime=[],
                Procs=[],
                StartPriority=[],
                SubmitTime=[],
                User=[]
                )
    for xml in jobs:
        data['State'].append(_get_val(xml.parentNode, ["option"]));
        data['JobName'].append(_get_val(xml,['JobName']));
        data['JobId'].append(_get_val(xml,['JobID'], dtype=str));
        data['WalltimeRequested'].append(_get_val(xml, ['ReqAWDuration'], dtype=int, default=0));
        data['WalltimeDuration'].append(_get_val(xml, ['AWDuration'], dtype=int, default=0, strict=False, warn=False));
        data['WalltimeRemaining'].append(data['WalltimeRequested'][-1] - data['WalltimeDuration'][-1]);
        data['StartTime'].append(_get_val(xml,['StartTime'], dtype=int));
        data['Procs'].append(_get_val(xml, ['ReqProcs'], dtype=int));
        data['StartPriority'].append(_get_val(xml,['StartPriority', 'RunPriority'], dtype=int, default=0, strict=False, warn=False));
        data['SubmitTime'].append(_get_val(xml,['SubmissionTime'], dtype=int, default=0, strict=False, warn=True));
        data['User'].append(_get_val(xml, ['User']));

    return pd.DataFrame(data);

def _parse_cluster(xmldata):
    cluster = xmldata.getElementsByTagName('cluster');
    ncpus = None
    t = None
    if len(cluster) >= 1:
        ncpus = int(cluster[0].attributes['LocalAllocProcs'].value);
        t = int(cluster[0].attributes['time'].value);
    return dict(cpu_count=ncpus, time=t);

def _fetch_data(mode, account, cache, completed=False):
    from xml.dom import minidom
    xmlstr = _query(mode, account, cache);
    data = minidom.parseString(xmlstr);
    jobdata = _parse_jobs(data);
    clusterdata = _parse_cluster(data);
    if completed and mode!='c':
        xmlstr = _query('c', account, cache);
        data = minidom.parseString(xmlstr)
        compdata = _parse_jobs(data);
        jobdata = pd.concat([jobdata, compdata], ignore_index=True);
    return jobdata, clusterdata;

def _fetch_test_data():
    from xml.dom import minidom
    import os
    datapath = os.path.join(os.path.dirname(__file__), "..", "test_data","all.xml")
    result = open(datapath).read();
    datapath = os.path.join(os.path.dirname(__file__), "..", "test_data","showq_completed.xml")
    result2 = open(datapath).read();
    dat = minidom.parseString(result)
    dat2 = minidom.parseString(result2)
    jobdata = _parse_jobs(dat);
    clusterdata = _parse_cluster(dat);
    jobdata2 = _parse_jobs(dat2);
    clusterdata2 = _parse_cluster(dat2);
    jobdata = pd.concat([jobdata, jobdata2], ignore_index=True);
    return jobdata, clusterdata

if __name__ == '__main__':
    print('Hello World')
    result = open("test_data/showq_active.xml").read();
    data = _parse_jobs(result);
    print(data)
