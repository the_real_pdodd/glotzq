"""
Helper functions to calculate some statistics about the queue
"""
import time as tm
import pandas as pd

def _sum_series(series, dtype):
    s = series.sum();
    s = dtype(s) if not pd.isnull(s) else dtype(0);
    return s;

def get_users(df):
    return sorted(set(df.get("User")));

def parse_jobid(jobid):
    if jobid.find('['):
        s = jobid.split('[')
        _id = int(s[0])
        _array = int(s[1].strip(']'))
    else:
        _id = int(jobid);
        _array = 0;
    return _id, _array;

def process_user(df, user, purge=43200, time=None):
    result = {
        "user": "",
        "activeHist": [],       # the number of procs <24, <48, 72+ hrs histogram
        "averageTimeRem":0,     # the average time remianing of the active jobs.
        "recentlyStarted": 0,   # the number of procs started in the last 12 hrs.
        "recentlyCompleted": 0, # the number of procs completed in the last 12 hrs.
        "recentlyQueued": 0,    # the number of procs submitted in the last 12 hrs.
        "numEligible": 0,       # the number of eligible procs the user has.
        "cpuHrsEligible": 0,    # the number of cpu hours that are eligible.
        "numBlocked": 0,        # the number of procs that are blocked.
        "cpuHrsBlocked": 0,     # the number of cpu hours that are blocked.
        "bestPriority": 0,      # the best start priority for that user.
    };

    udf = df.loc[df['User'] == user];
    active = udf.loc[udf['State'] == 'active'].sort_values(by="WalltimeRemaining");
    idle = udf.loc[udf['State'] == 'eligible'];
    blocked = udf.loc[udf['State'] == 'blocked'];
    completed = udf.loc[udf['State'] == 'completed'];

    remaining = active["WalltimeRemaining"]
    procs = active["Procs"]
    cputimeRemaining = remaining.mul(procs);
    avgRemTime = 0.0;
    if active["Procs"].sum() > 0:
        avgRemTime = cputimeRemaining.sum()/float(active["Procs"].sum())/3600.0;
    cprocs = 0;
    now = procs.sum();
    n24 = procs.where(remaining >= 24*3600).sum()
    n24 = int(n24) if not pd.isnull(n24) else 0
    n48 = procs.where(remaining >= 48*3600).sum()
    n48 = int(n48) if not pd.isnull(n48) else 0
    n72 = procs.where(remaining >= 72*3600).sum()
    n72 = int(n72) if not pd.isnull(n72) else 0
    hist = [now, n24, n48, n72];
    if time is None:
        time = tm.time();
    nstarted=udf["Procs"].where(udf["StartTime"] > (time-purge)).sum()
    nstarted=int(nstarted) if not pd.isnull(nstarted) else 0
    nqueued=udf["Procs"].where(udf["SubmitTime"] > (time-purge)).sum()
    nqueued=int(nqueued) if not pd.isnull(nqueued) else 0

    result["user"]=user;
    result["activeHist"]=hist;
    result["averageTimeRem"]=avgRemTime;
    result["recentlyStarted"]=nstarted;
    result["recentlyCompleted"]=int(completed["Procs"].sum());
    result["recentlyQueued"] = nqueued;
    result["numEligible"]=int(idle["Procs"].sum());
    result["cpuHrsEligible"]=(idle["Procs"].mul(idle["WalltimeRequested"]).sum())/3600.0;
    result["numBlocked"]=int(blocked["Procs"].sum());
    result["cpuHrsBlocked"]=(blocked["Procs"].mul(blocked["WalltimeRequested"]).sum())/3600.0;
    priority = idle["StartPriority"].max()
    priority = int(priority) if not pd.isnull(priority) else 0;
    result["bestPriority"]=priority;
    return result;

def get_num_procs(df, state):
    if len(df['State'])==0:
        return 0;
    return _sum_series(df.loc[df['State'] == state]["Procs"], dtype=int);

def get_backlog(df, numcpus):
    idle = df.loc[df['State'] == 'eligible']
    blocked = df.loc[df['State'] == 'blocked']
    idle_hrs = _sum_series(idle["Procs"].mul(idle["WalltimeRequested"]), dtype=float)/3600.0;
    blocked_hrs = _sum_series(blocked["Procs"].mul(blocked["WalltimeRequested"]), dtype=float)/3600.0;
    return int((idle_hrs+blocked_hrs)/float(numcpus)/24.0);


def process_cluster(df, numcpus):
    """
    returns the number of
    """
    return {
        "active":get_num_procs(df, state='active'),
        "eligible":get_num_procs(df, state='eligible'),
        "blocked":get_num_procs(df, state='blocked'),
        "backlog":get_backlog(df, numcpus),
    };
