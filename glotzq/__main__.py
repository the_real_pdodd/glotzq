"""
The main entry point to the program.
"""
import argparse
import sys
import os
import logging
import pandas
from datetime import datetime
import time

from . import scheduler
from . import data
from . import database as db
from . import config
from .report import generate_report, hipchat_report
import glotzq

logger = logging.getLogger("glotzq.{}".format(__name__));

def run(full, no_cache, latest, database, cpu, gpu, loglevel=None, hipchat=False):
    if loglevel is not None:
        config.logger.setLevel(loglevel);
    logger.info("glotzq v{}".format(glotzq.__version__));
    config_data = config.read_config();
    connected = False
    if database:
        logger.info("testing database connection")
        sleepcount = 25;
        for _ in range(sleepcount):
            if not db.test_connection():
                logger.error("Could not connect to database trying again in 1 min...")
                time.sleep(60)
            else:
                connected = True;
                break
        if not connected:
            logger.error("Could not connect to database. Please try again later.");

    if not config.is_valid():
        logger.warning("could not find \'showq\' -- reporting the latest results from the database");
        latest=True;

    msg = "cache"
    if no_cache:
        msg = "scheduler"
    if latest:
        msg = "database"
    logger.info("Fetching data from {}...".format(msg));

    jobdata = None;
    jobdata1 = None;
    clusterdata = None;
    clusterdata1 = None;
    if latest and connected:
        database=False # prevent reading data an writing it directly back
        docs = list(db.retrieve_latest(account="sglotzer_fluxoe", collection=db.glotzq_collection, limit=1))
        doc = None
        if len(docs) == 1:
            doc = docs[0]
        else:
            raise RuntimeError("Error fetching data from database.");

        if not db.verify_doc(doc):
            raise RuntimeError("Document does not match the schema.");

        jobdata = pandas.DataFrame(doc['jobs']);
        clusterdata = dict(cpu_count=592, time=doc['time']);
        if gpu:
            docs = list(db.retrieve_latest(account="sglotzer1_fluxoe", collection=db.glotzq_collection, limit=1))
            doc = None
            if len(docs) == 1:
                doc = docs[0]
            else:
                raise RuntimeError("Error fetching data from database.");

            if not db.verify_doc(doc):
                raise RuntimeError("Document does not match the schema.");

            jobdata1 = pandas.DataFrame(doc['jobs']);
            clusterdata1 = dict(cpu_count=64, time=doc['time']);
    elif latest and not connected:
        logger.warning("could not connect to database falling back to test data");
        jobdata, clusterdata = scheduler._fetch_test_data();
        gpu = False;
    else:
        jobdata, clusterdata = scheduler._fetch_data(mode=None, account="sglotzer_fluxoe", cache=(not no_cache), completed=full);
        if gpu:
            jobdata1, clusterdata1 = scheduler._fetch_data(mode=None, account="sglotzer1_fluxoe", cache=(not no_cache), completed=full);

    if database:
        if gpu:
            db.upload(
                    entry=db.format(account="sglotzer1_fluxoe", jobs=jobdata1, cluster=clusterdata1, nodes={}),
                    collection=db.glotzq_collection
                    );
        if cpu:
            db.upload(
                    entry=db.format(account="sglotzer_fluxoe", jobs=jobdata, cluster=clusterdata, nodes={}),
                    collection=db.glotzq_collection
                    );



    logger.info("\n******** glotzq report as of {date} ********\n".format(date=datetime.fromtimestamp(clusterdata['time']).strftime('%c')));
    if gpu:
        if hipchat:
            hipchat_report(name="GPU Nodes", df=jobdata1, numcpus=64, token=config_data['hipchat_token'], roomid=config_data['hipchat_roomid']);
        generate_report("GPU Nodes", jobdata1, clusterdata1, verbose=full, ncpus=64);
    if cpu:
        if hipchat:
            hipchat_report(name="CPU Nodes", df=jobdata, numcpus=592, token=config_data['hipchat_token'], roomid=config_data['hipchat_roomid']);
        generate_report("CPU Nodes", jobdata, clusterdata, verbose=full, ncpus=592);

    return 0;

def main():
    parser = argparse.ArgumentParser(
        description="Query the scheduler and print the results to standard out."
        )
    parser.add_argument(
        '--full',
        action='store_true',
        help="The long version of the scheduler report")
    parser.add_argument(
        '--no-cache',
        action='store_true',
        help="Don't use the cache when querying the scheduler. May result in longer return times.")
    parser.add_argument(
        '--latest',
        action='store_true',
        help="Use the last result stored in the database.")
    parser.add_argument(
        '--cpu',
        action='store_false',
        dest='gpu',
        help="Generate report for the cpu queue")
    parser.add_argument(
        '--gpu',
        action='store_false',
        dest='cpu',
        help="Generate report for the gpu queue")
    parser.add_argument(
        '--database',
        action='store_true',
        help="Save the result in the database")
    parser.add_argument(
        '--hipchat',
        action='store_true',
        help="Ping hipchat with a short report")

    args = parser.parse_args()
    # db.list_entries(account='sglotzer1_fluxoe', collection=db.glotzq_collection)
    return run(**vars(args), loglevel=logging.INFO); #


if __name__ == '__main__':
    sys.exit(main());
