"""
Helper functions to generate reports.
"""
import logging
from datetime import datetime, timedelta
from .util.tabulate import tabulate
from .util import hipchat
from . import data

logger = logging.getLogger("glotzq.{}".format(__name__));

def _pad_part(string, char, size):
    psz = size - len(string);
    pad = ""
    if psz > 0:
        pad = char*psz;
    return pad;

def _pad_right(string, char, size):
    pad = _pad_part(string, char, size);
    return string+pad;

def _pad_left(string, char, size):
    pad = _pad_part(string, char, size);
    return pad+string;

def report_recent(userdata):
    tdata  = [];
    for user in userdata:
        if  user["recentlyStarted"]+user["recentlyCompleted"]+user["recentlyQueued"] > 0:
            tdata.append([user["user"], user["recentlyStarted"], user["recentlyCompleted"], user["recentlyQueued"]])
    logger.info(tabulate(tdata, headers=("user", "started", "completed", "submitted"), tablefmt="simple",
                 floatfmt="g", numalign="center", stralign="left",
                 missingval="?"));


def report_active(userdata, numcpus):
    tdata  = [];
    totalTime = 0;
    totalHist = [0,0,0,0]
    name_len = 0;
    for user in userdata:
        if user["activeHist"][0] > 0:
            name_len = max(name_len, len(user["user"]));
            totalTime+= user["activeHist"][0]*user["averageTimeRem"];
            for i in range(len(totalHist)):
                totalHist[i]+= user["activeHist"][i]
            tdata.append([user["user"]] + user["activeHist"] + [round(user["averageTimeRem"],1)])
    line = []
    for sz in [name_len, 9, 5, 5, 5, 6]:
        line.append(_pad_right("", char="-", size=sz))
    tdata.append(line)
    histtab = round(totalTime/float(totalHist[0]),1) if totalHist[0] != 0 else 0
    tdata.append(["total"]+totalHist+[histtab]);
    logger.info(tabulate(tdata, headers=("user", "running", ">1d", ">2d", ">3d", "mean"), tablefmt="simple",
                 floatfmt="g", numalign="left", stralign="left",
                 missingval="?"));

# def report_queued(userdata, numcpus):
#     tdata  = [];
#     totalQueued = 0.0;
#     for user in userdata:
#         if  user["numEligible"]+user["numBlocked"] > 0:
#             totalQueued+=user["cpuHrsEligible"]+user["cpuHrsBlocked"];
#             tdata.append([user["user"], user["numEligible"], user["numBlocked"], round(user["cpuHrsEligible"]+user["cpuHrsBlocked"], 1), user["bestPriority"]])
#     tdata = sorted(tdata, key=lambda x:x[4], reverse=True);
#     logger.info(tabulate(tdata, headers=("user", "eligible procs", "blocked procs", "req. cpu hrs", "priority"), tablefmt="simple",
#                  floatfmt="g", numalign="center", stralign="left",
#                  missingval="?"));
#     logger.info("***backlog: {} days.".format(round(totalQueued/float(numcpus*24),1)));

def report_queued(userdata, numcpus):
    tdata  = [];
    totalQueued = 0.0;
    for user in userdata:
        if  user["numEligible"]+user["numBlocked"] > 0:
            totalQueued+=user["cpuHrsEligible"]+user["cpuHrsBlocked"];
            tdata.append([user["user"], user["numEligible"], user["numBlocked"], round(user["cpuHrsEligible"]+user["cpuHrsBlocked"], 1), user["bestPriority"]])
    tdata = sorted(tdata, key=lambda x:x[4], reverse=True);
    logger.info(tabulate(tdata, headers=("user", "eligible procs", "blocked procs", "req. cpu hrs", "priority"), tablefmt="simple",
                 floatfmt="g", numalign="center", stralign="left",
                 missingval="?"));
    logger.info("***backlog: {} days.".format(round(totalQueued/float(numcpus*24),1)));


def generate_report(name, jobdata, clusterdata, verbose, ncpus):
    szTotal = 75
    users = data.get_users(jobdata);
    userdata = []
    nactive = 0;
    for user in users:
        userdata.append(data.process_user(jobdata, user, time=clusterdata["time"]));
        nactive += userdata[-1]["activeHist"][0];
    header = "{}: {} active/{} total".format(name, nactive, ncpus);
    header = _pad_right(header, "-", szTotal);
    logger.info(header);
    report_active(userdata, numcpus=ncpus);
    if verbose:
        header = "Recent Activity (last 12 hrs) ";
        header = _pad_right(header, "-", szTotal);
        logger.info("\n");
        logger.info(header);
        report_recent(userdata);

        header = "Queued jobs summary";
        header = _pad_right(header, "-", szTotal);
        logger.info("\n");
        logger.info(header);
        report_queued(userdata, numcpus=ncpus);
    logger.info("");

def hipchat_report(name, df, numcpus, token, roomid):
    if len(token)!=40 or roomid == 0:
        logger.info("Could not send notification to room id {} with token length {}".format(roomid, len(token)))
        return;
    summary = data.process_cluster(df, numcpus);
    tdata=[];
    tdata.append(['active', summary['active'], "procs"])
    tdata.append(['queued', summary['eligible']+summary['blocked'], "procs"])
    tdata.append(['backlog', summary['backlog'], "days"])
    table = tabulate(tdata, tablefmt="html",
                 floatfmt="g", numalign="left", stralign="left",
                 missingval="?")
    # a, b, i, strong, em, br, img, pre, code, lists, tables
    title = "<br><strong>Summary of jobs on {n}  ({timestr})</strong></br>\n".format(n=name, timestr=datetime.now().strftime("%c"));
    # logger.info(title+table)
    hipchat.notification(
                        message=title+table,
                        token=token,
                        roomid=roomid,
                        send_from="glotzq",
                        message_format="html");
