import requests
import json

def notification(message, token, roomid, notify=False, color="gray", send_from=None, message_format='text'):
    """
    https://www.hipchat.com/docs/apiv2/method/send_room_notification
    TODOs:
    validate input as per the specifications from the api website.
    add documentation.
    """
    url = "https://api.hipchat.com/v2/room/{}/notification".format(roomid)
    header = {"Content-Type": "application/json", "Authorization" : "Bearer {}".format(token)};
    parameters= {
        'message': message,
        'notify': notify,
        'message_format': message_format,
        'color': color
    };
    if send_from is not None:
        parameters["from"] = send_from;
    result = requests.post(url, data=json.dumps(parameters), headers=header);
    result.raise_for_status();
