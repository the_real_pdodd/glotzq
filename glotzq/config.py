"""
test the system config.
"""
import subprocess
import logging

# class GlotzqFormatter(logging.Formatter):
#     def __init__(self, fmt):
#         logging.Formatter.__init__(self,fmt);
#         print(dir(self))
#         self._default = fmt;
#         print(self._fmt)
#
#     def format(self, record):
#         print ("format = ", self._fmt)
#         print(dir(record))
#         print('ok- {} {}'.format(record.levelno, logging.INFO))
#         self._fmt = '[%(filename)s:%(lineno)d]%(levelname)s - %(message)s'
#         if record.levelno >= logging.INFO:
#             pass
#             '[%(asctime)s] p%(process)s {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s'
#         else:
#             self._fmt = '%(message)s'
#         s = super().format(record);
#         print ("format = ", self._fmt)
#         print(s)
#         return s

logger = logging.getLogger("glotzq")
formatter = logging.Formatter('%(message)s')
logger.setLevel(logging.WARNING)
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
logger.addHandler(stream_handler)

def _find_command(c):
    cmd = ['which', c]
    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE);
    path = proc.stdout.read().strip();
    error = proc.stderr.read().strip();
    if len(error) == 0:
        if len(path) == 0:
            return None;
        else:
            return path;
    raise RuntimeError("Error occured while searching for {}: {}".format(c, error))

def is_valid():
    return _find_command("showq") is not None;

def write_config(token, roomid):
    import os
    import json
    home = os.path.expanduser('~');
    with open(os.path.join(home, '.glotzq'), 'w') as cfile:
        cfile.write(json.dumps({"hipchat_token":token, "hipchat_roomid":roomid}, indent=4)+'\n')


def read_config():
    import os
    import json
    config=dict(hipchat_token="", hipchat_roomid=0)
    cread = None;
    home = os.path.expanduser('~');
    if  os.path.exists(os.path.join(home, '.glotzq')):
        with open(os.path.join(home, '.glotzq'), 'r') as cfile:
            cread = json.loads(cfile.read());
        if cread is not None:
            for key in config:
                if key in cread:
                    config[key] = cread[key];
    return config;
