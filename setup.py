import sys
from setuptools import setup, find_packages

if sys.version_info < (2, 7, 0):
    print("Error: glotzq requires python version >= 2.7.x.")
    sys.exit(1)

setup(
    name='glotzq',
    version='0.1.0',
    packages=find_packages(),
    zip_safe=True,
    author='Paul M Dodd',
    author_email='pdodd@umich.edu',
    description="wrapper to showq commands",
    url="https://bitbucket.org/the_real_pdodd/glotzq",
    install_requires=['pandas', 'pymongo', 'signac', 'requests'], #  install_requires or something else?
    entry_points={
        'console_scripts': [
            'glotzq = glotzq.__main__:main',
        ],
    },
)
