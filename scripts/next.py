from datetime import datetime, timedelta
import time

class Schedule(object):

    def __init__(self, start, interval):
        if not isinstance(start, datetime):
            raise TypeError("Schedule.__init__: {!r} must be a datetime object".format('start'));
        if not isinstance(interval, timedelta):
            raise TypeError("Schedule.__init__: {!r} must be a timedelta object".format('interval'));
        self._start = start;
        self._interval = interval;

    def next_date(self, now=None):
        if now is None:
            now = datetime.now();
        dt = now-self._start;
        n = int(dt/self._interval);
        return self._start + (n+1)*self._interval;

    def next_seconds(self, now=None):
        if now is None:
            now = datetime.now();
        dt = self.next_date(now) - now;
        return int(dt.total_seconds())+1;

if __name__ == '__main__':
    schedule = Schedule(start=datetime(year=1988, month=10, day=18, hour=6, minute=30), interval=timedelta(hours=12))
    future = schedule.next_date();
    print("{hh}{mm}".format(hh=str(future.hour).zfill(2), mm=str(future.minute).zfill(2)))
