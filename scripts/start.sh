# launch the process
if [[ -f glotzq.pid ]]; then
    echo "glotzq.pd has been found. remove this file and try again."
    exit -1;
fi
nohup python infinite.py &
echo "waiting for process to start..."
while [[ ! -f glotzq.pid ]]; do
    sleep 1
done
PID=`head -n 1 glotzq.pid`
NODE=`tail -n 1 glotzq.pid`
echo "Starting process @ $PID on $NODE"
