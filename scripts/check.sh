# kill the process.
if [[ ! -f glotzq.pid ]]; then
    echo "could not find glotzq.pid! no actions taken"
    exit -1;
fi
PID=`head -n 1 glotzq.pid`
NODE=`tail -n 1 glotzq.pid`
if [[ $HOSTNAME == $NODE ]]; then
    ps --pid $PID
else
    echo "glotzq is not running on this host ($HOSTNAME) try again on $NODE"
fi
