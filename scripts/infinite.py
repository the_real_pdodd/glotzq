"""
monitor the queue until the end of time.
"""
from datetime import datetime, timedelta
import time
import os
import platform
import glotzq
import logging

pid = os.getpid();
# setup the logger.
logger = logging.getLogger('infinite')
logger.setLevel(level=logging.INFO)
logFormatter = logging.Formatter("%(asctime)s - %(levelname)-5.5s:  %(message)s")
fileHandler = logging.FileHandler("glotzq.log")
fileHandler.setFormatter(logFormatter)
logger.addHandler(fileHandler)
consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
logger.addHandler(consoleHandler)

class Schedule(object):

    def __init__(self, start, interval):
        if not isinstance(start, datetime):
            raise TypeError("Schedule.__init__: {!r} must be a datetime object".format('start'));
        if not isinstance(interval, timedelta):
            raise TypeError("Schedule.__init__: {!r} must be a timedelta object".format('interval'));
        self._start = start;
        self._interval = interval;

    def next_date(self, now=None):
        if now is None:
            now = datetime.now();
        dt = now-self._start;
        n = int(dt/self._interval);
        return self._start + (n+1)*self._interval;

    def next_seconds(self, now=None):
        if now is None:
            now = datetime.now();
        dt = self.next_date(now) - now;
        return int(dt.total_seconds())+1;

def infinite():
    logger.info("Starting infinite loop @ {} on {}".format(pid, platform.node()))
    with open('glotzq.pid', 'w') as pfile:
        pfile.write(str(pid)+"\n"+platform.node()+"\n");
    schedule = Schedule(start=datetime(year=1988, month=10, day=18, hour=0, minute=30), interval=timedelta(hours=8))
    try:
        while True:
            current_time = datetime.now();
            future = schedule.next_date(now=current_time);
            sleep_duration = schedule.next_seconds(now=current_time);
            logger.info("Next run is scheduled for {} (in {} seconds)".format(future, sleep_duration))
            logger.info("sleeping for {} seconds".format(sleep_duration));
            time.sleep(sleep_duration);
            glotzq.run(full=True, no_cache=True, latest=False, database=True, cpu=True, gpu=True, hipchat=True);
    except:
        logger.info("Exiting infinte loop");

if __name__ == '__main__':
    infinite();
