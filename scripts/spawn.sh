#PBS -N glotzq
#PBS -l nodes=1,walltime=00:30:00
#PBS -A sglotzer_fluxoe
#PBS -l gres=cpuslots
#PBS -l qos=flux
#PBS -q fluxoe
#PBS -j oe
#PBS -p 1023
#PBS -V

SRC=/home/pdodd/Code/glotzq/scripts
cd /home/pdodd/glotzq 
HIPCHAT=""
count=`ls -1q glotzq* | wc -l` # counts the number of log file from the previous qsub commands.
if [[ $(( $count % 2 )) -eq 0 ]]
then 
	HIPCHAT="--hipchat"
fi 
echo $HIPCHAT
glotzq --full --database $HIPCHAT
next=`python $SRC/next.py`
qsub -a $next $SRC/spawn.sh

