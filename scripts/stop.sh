# kill the process.
if [[ ! -f glotzq.pid ]]; then
    echo "could not find glotzq.pid! no actions taken"
    exit -1;
fi
PID=`head -n 1 glotzq.pid`
NODE=`tail -n 1 glotzq.pid`
if [[ $HOSTNAME == $NODE ]]; then
    echo "Killing process @ $PID on $NODE"
    kill $PID
else
    echo "Error! host($HOSTNAME) does not match the host found($NODE)"
fi
