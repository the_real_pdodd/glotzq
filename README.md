# glotzq #

Query the scheduler and print the results to standard out. This has only been tested on flux.


### How do I get set up? ###

This package requires pandas and signac.
```
$ pip install pandas --user
$ pip install signac --user
```
Then install this package.
```
$ cd path/to/code
$ python setup.py install --user
```
Add ```export PATH=${HOME}/.local/bin:$PATH``` to the end of your
.bashrc or .bash_profile variable

### Usage ###
```
usage: glotzq [-h] [--full] [--no-cache] [--latest] [--cpu] [--gpu]
              [--database] [--hipchat]

Query the scheduler and print the results to standard out.

optional arguments:
  -h, --help  show this help message and exit
  --full      The long version of the scheduler report
  --no-cache  Don't use the cache when querying the scheduler. May result in
              longer return times.
  --latest    Use the last result stored in the database.
  --cpu       Generate report for the cpu queue
  --gpu       Generate report for the gpu queue
  --database  Save the result in the database
  --hipchat   Ping hipchat with a short report (if configured)
```

### Configure hipchat notifications ###
Meant for the person running the submit script on flux.
```
./configure TOKEN ROOMID
```
Where TOKEN is your hipchat token (see below), and ROOMID is the hipchat room id, which can be found by logging on to hipchat via a browser and finding the desired room (I couldn't find the room id via the app).

Here is the usage for configure.
```
usage: configure [-h] token roomid

configure glotzq for hipchat notifications.

positional arguments:
  token       your user hipchat v2 api token. See
              https://developer.atlassian.com/hipchat/guide/hipchat-rest-
              api/api-access-tokens#APIaccesstokens-Usergeneratedtokens for
              more information.
  roomid      room id for the hipchat room.

optional arguments:
  -h, --help  show this help message and exit
```
